/*
 * Morandi Riccardo 5Itel 17/11/2021 
 * Programma di test 
 */

/* 
 *  incremeno di c
 *  funzione LM  
 *  float: 
 *  n=analogread pin lm
 *  v = vcc/4095  *n
 *  t =v/0.01
 *  
 *  stampo una stringa di spazi per cancellare quello che c'è già 
 */
#include <LiquidCrystal.h>
LiquidCrystal lcd(25, 26, 19, 21, 22, 23);
#define led 18
#define lm 4
#define t1 16
#define t2 17

int c=0;
float vcc = 5.0;
float t = 0.0;

void setup() {
 pinMode (lm, INPUT);
 pinMode (t1, INPUT);
 pinMode (t2, INPUT);
 pinMode (led, OUTPUT);
 lcd.begin(16, 2);
 accensione_led();
}

void loop() {
 
  prova_tasti();
  scrittura_lcd();
  lcd.clear();
  //LM35();
}

void accensione_led(){
    digitalWrite(led, HIGH);
  delay (1000);

  digitalWrite(led, LOW);
  delay (1000);

  digitalWrite(led, HIGH);
  delay (1000);

  digitalWrite(led, LOW);
  delay (1000);
  }

void prova_tasti(){
  if (digitalRead(t1)){
    digitalWrite(led, HIGH);
    delay(200);
    while(!digitalRead(t1)){
      }
      delay (2000);
    digitalWrite(led, LOW);
  } 
  if (digitalRead(t2)){
    digitalWrite(led, HIGH);
    delay(200);
     while(!digitalRead(t2)){
      }
      delay (2000);
    digitalWrite(led, LOW);
  }
}

void scrittura_lcd(){
    lcd.print("Morandi 5Itel");
    lcd.setCursor(0,1);
    lcd.print("termostato");
    //lcd.print(c);
  }
/*
void LM35(){
  lcd.clear();
  lcd.print(analogRead(4));
  }*/
