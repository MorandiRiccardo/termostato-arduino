/* Bonazzi Giorgio 5itel 2021
   * Programma termostato con ESP32 e LM35
   * 
   */
  
  #include <LiquidCrystal.h>
  
  float Term = 26.0;
  float Vcc = 1.1; //legato all'attenuazione (riduco la scala da 0 to 5 a 0 to 1.1 e aumento la risolione)
  float T = 0.0;
  const int analogPin = 34;
  const int ledPin = 18;
  const int tasto1 = 16;
  const int tasto2 = 17;
  LiquidCrystal lcd(25,26,19,21,22,23); // lcd(RS,EN,D4,D5,D6,D7);

  

  void setup() {
    analogSetPinAttenuation(analogPin,ADC_0db); // ADC_0db Attenuation 1, full scale 1,1V - ADC_6db Attenuation 2, full scale 2,2V
    pinMode (ledPin, OUTPUT);
    pinMode(tasto1, INPUT);  //TASTO down
    pinMode(tasto2, INPUT);  //TASTO up
    lcd.begin(16, 2);
    lcd.display();
    lcd.print("Term          T");
        
  }

  void loop() {
    if (digitalRead(tasto1) == LOW){
    Term=Term-1;
        
    delay(200);
  
    while(!digitalRead(tasto1)){
    }
    delay(200);
    }
    if (digitalRead(tasto2) == LOW){
    Term=Term+1;
        
    delay(200);
  
    while(!digitalRead(tasto2)){
    }
    delay(200);
    }
    analogSetPinAttenuation(analogPin,ADC_0db);
    //int sensorReading = analogRead(analogPin);
    float N = analogRead(analogPin);
    float V = (Vcc/4095.0)*N;
    float T = (V/0.01);
    lcd.setCursor(0,1);
    lcd.print("      ");
    lcd.setCursor(0,1);
    lcd.print(Term);
    
    lcd.setCursor(12,1);
    lcd.print(T);
    delay(500);
    if (T >= Term) {
   digitalWrite(ledPin,LOW);
  }
  else if (T < Term) {
    digitalWrite(ledPin,HIGH);
  }
  }
